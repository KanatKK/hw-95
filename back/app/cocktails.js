const router = require("express").Router();
const Cocktail = require("../models/Cocktail");
const User = require("../models/User");
const path = require("path");
const config = require("../config");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const multer = require("multer");
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req,res) => {
    try {
        const cocktails = await Cocktail.find({published: true});
        res.send(cocktails);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post("/",upload.single("image"), auth, async (req,res) => {
    try {
        if (req.body.name === "") {
            return res.status(400).send({error: "Please, add cocktail name"});
        }
        if (req.body.recipe === "") {
            return res.status(400).send({error: "Please, add recipe"});
        }
        const ingredients = JSON.parse(req.body.ingredients);
        ingredients.map(ing => {
            if (ing.name === "") {
                return res.status(400).send({error: "Please, add ingredient name."});
            }
            if (ing.amount === "") {
                return res.status(400).send({error: "Please, add amount of ingredient."});
            }
        });
        const cocktailData = req.body;
        if (req.file) {
            cocktailData.image = req.file.filename;
        }
        const cocktail = new Cocktail(cocktailData);
        cocktail.generateAuthor(req.author._id);
        cocktail.getIngredients(ingredients);
        await cocktail.save();
        res.send({message: "Your cocktail is being reviewed by the administrators"});
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get("/myCocktails/:id", auth, async (req,res) => {
    try {
        const cocktails = await Cocktail.find({author: req.params.id});
        res.send(cocktails);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.get("/unpublished", [auth, permit('admin')], async (req,res) => {
    try {
        const unpublishedCocktails = await Cocktail.find({"published": false});
        res.send(unpublishedCocktails);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.put("/publish/:id", [auth, permit('admin')], async (req, res) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);
        cocktail.published = true;
        await cocktail.save();
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/delete/:id", [auth, permit('admin')], async (req,res) => {
    try {
        await Cocktail.findByIdAndDelete(req.params.id);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;