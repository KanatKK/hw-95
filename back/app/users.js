const router = require("express").Router();
const User = require("../models/User");
const config = require("../config");
const axios = require("axios");
const {nanoid} = require("nanoid");

router.get("/", async (req, res) => {
    try {
        const users = await User.find();
        res.send(users);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", async (req, res) => {
    try {
        const checkUser = await User.findOne({username: req.body.username});
        if (checkUser) {
            return res.status(400).send({error: "This username is already taken"});
        }
        if (req.body.username === "") {
            return res.status(400).send({error: "Please, add Username"});
        }
        if (req.body.password === "") {
            return res.status(400).send({error: "Please, add Password"});
        }
        if (req.body.displayName === "") {
            return res.status(400).send({error: "Please, add Display name"});
        }
        const user = new User({
            username: req.body.username,
            password: req.body.password,
            displayName: req.body.displayName,
            imageLink: req.body.imageLink,
        });
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post("/facebookLogin", async (req,res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.fb.appId + "|" + config.fb.appSecret;
    const debugToken = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
    try {
        const response = await axios.get(debugToken);
        if (response.data.data.error) {
            return res.status(401).send({message: "Facebook token incorrect"});
        }
        if (response.data.data.user_id !== req.body.id) {
            return res.status(401).send({message: "Wrong user ID"});
        }
        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                username: req.body.name,
                password: nanoid(),
                facebookId: req.body.id,
                imageLink: req.body.picture.data.url,
                displayName: req.body.name,
            });
        }
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(401).send({message: "Facebook token incorrect"});
    }
});

router.post("/sessions", async (req, res) => {
    const user = await User.findOne({username: req.body.username});
    if (!user) {
        return res.status(400).send({error: "Username not found"});
    }
    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) {
        return res.status(400).send({error: "Password is wrong"});
    }
    user.generateToken();
    await user.save();
    res.send(user);
});

router.delete("/sessions", async (req, res) => {
    const token = req.get("Authorization");
    const success = {message: "Success"};

    if (!token) return res.send(success);
    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();
    user.save();

    return res.send(success);
});

module.exports = router;