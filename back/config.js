const path = require("path");
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public/uploads"),
    db: {
        name: "cocktails",
        url: "mongodb://localhost",
    },
    fb: {
        appId: "861692451283488",
        appSecret: "0375d9ba91e5d384b621f68689512919",
    },
};