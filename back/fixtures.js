const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const {nanoid} = require("nanoid");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});
const db = mongoose.connection;

db.once("open", async () => {
    try  {
        await db.dropCollection('users');
    } catch (e) {
        console.log("Collection were not presented, skipping drop...");
    }

    await User.create({
        username: "admin",
        password: "4321",
        role: "admin",
        displayName: "Rick",
        imageLink: "https://avatarfiles.alphacoders.com/128/128984.png",
        token: nanoid(),
    })
    db.close();
});