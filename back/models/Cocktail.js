const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CocktailSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    recipe: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    ingredients: {
        type: Array,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    published: {
        type: Boolean,
    }
});

CocktailSchema.methods.generateAuthor = function (author) {
    this.author = author;
};

CocktailSchema.methods.getIngredients = function (ingredients) {
    this.ingredients = ingredients
}

const Cocktail = mongoose.model("Cocktail", CocktailSchema);

module.exports = Cocktail;