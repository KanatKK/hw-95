import React from 'react';
import FacebookLoginButton from "react-facebook-login/dist/facebook-login-render-props";
import {fbAppId} from "../../constants";
import {useDispatch} from "react-redux";
import {facebookLogin} from "../../store/actions";

const FacebookLoginBtn = () => {
    const dispatch = useDispatch();
    const facebookResponse = response => {
        if (response.id) {
            dispatch(facebookLogin(response));
        }
    };

    return <FacebookLoginButton
        appId={fbAppId}
        fields="name,email,picture"
        render={renderProps => {
            return <button
                onClick={renderProps.onClick}
                className="facebookBtn"
            >
                Enter with facebook
            </button>
        }}
        callback={facebookResponse}
    />
};

export default FacebookLoginBtn;