import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteCocktail} from "../../store/actions";

const CocktailsList = () => {
    const dispatch = useDispatch();
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const user = useSelector(state => state.user.user);

    const deleteCocktailHandler = event => {
        dispatch(deleteCocktail(event.target.id));
    };

    if (cocktails) {
        const cocktailsList = cocktails.map((cocktail, ind) => {
            return (
                <div className="cocktail" key={ind}>
                    <img src={'http://localhost:8000/uploads/' + cocktail.image} alt=""/>
                    <div className="info">
                        <h4>{cocktail.name}</h4>
                        <ul>
                            {cocktail.ingredients.map((ing, ind) => {
                                return (
                                    <li key={ind}>{ing.name} - {ing.amount}</li>
                                )
                            })}
                        </ul>
                        <p>{cocktail.recipe}</p>
                    </div>
                    {   user && user.role === "admin" &&
                        <div className="buttons">
                            <button
                                id={cocktail._id} onClick={deleteCocktailHandler}
                                style={{marginRight: 5}}
                            >Delete</button>
                        </div>
                    }
                </div>
            );
        });
        return (
            <div className="cocktails">
                {cocktailsList}
            </div>
        );
    } else {
        return null;
    }
};

export default CocktailsList;