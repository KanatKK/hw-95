import React from 'react';

const IngredientLine = (props) => {
    return (
        <div className="individualIngredient">
            <input
                type="text" className="ingName"
                placeholder="Ingredient Name" value={props.name}
                onChange={props.onIngNameChange}
            />
            <input
                type="text" className="ingAmount"
                placeholder="Amount" value={props.amount}
                onChange={props.onIngAmountChange}
            />
            {
                props.deleteBtn &&
                <button type="button" onClick={props.remove} className="deleteIngBtn">X</button>
            }
        </div>
    );
};

export default IngredientLine;