import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Nav from "../Nav/Nav";
import {getUnpublishedCocktails} from "../../store/actions";
import UnpCocktailsList from "../../components/UnpCocktailsList/UnpCocktailsList";

const UnpublishedCocktails = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);
    if (user.role !== "admin") {
        props.history.push("/");
    }

    useEffect(() => {
        dispatch(getUnpublishedCocktails());
    }, [dispatch]);

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(getUnpublishedCocktails());
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch]);

    return (
        <div className="container">
            <header>
                <h2>Unpublished</h2>
                <Nav
                    path="/" name="Cocktails"
                />
            </header>
            <div className="content">
                <UnpCocktailsList/>
            </div>
        </div>
    );
};

export default UnpublishedCocktails;