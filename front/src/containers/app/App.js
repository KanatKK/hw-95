import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Login from "../Registration/Login";
import MainPage from "../MainPage/MainPage";
import CreateCocktail from "../CreateCocktail/CreateCocktail";
import UnpublishedCocktails from "../UnpublishedCocktails/UnpublishedCocktails";
import MyCocktails from "../MyCocktails/MyCocktails";

const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={MainPage}/>
              <Route path="/login" exact component={Login}/>
              <Route path="/createCocktail" exact component={CreateCocktail}/>
              <Route path="/unpublished" exact component={UnpublishedCocktails}/>
              <Route path="/myCocktails" exact component={MyCocktails}/>
          </Switch>
      </BrowserRouter>
  );
};

export default App;
