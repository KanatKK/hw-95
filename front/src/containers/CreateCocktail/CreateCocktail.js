import React, {useState} from 'react';
import Nav from "../Nav/Nav";
import IngredientLine from "../../components/IngredientLine/IngredientLine";
import {useDispatch, useSelector} from "react-redux";
import {createCocktail} from "../../store/actions";

const CreateCocktail = () => {
    const dispatch = useDispatch();
    const errors = useSelector(state => state.cocktails.errors);
    const message = useSelector(state => state.cocktails.message);
    const [ingredients, setIngredients] = useState([{name: '', amount: '', deleteBtn: false},])
    const [cocktail, setCocktail] = useState({
        name: '', recipe: '', image: null, published: false,
    });

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(cocktail).forEach(key => {
            formData.append(key, cocktail[key]);
        });
        formData.append('ingredients', JSON.stringify(ingredients));
        dispatch(createCocktail(formData));

        const cocktailCopy = {...cocktail};
        cocktailCopy.name = "";
        cocktailCopy.recipe = "";
        cocktailCopy.image = null;
        cocktailCopy.published = false;
        setCocktail(cocktailCopy);
        setIngredients([{name: '', amount: '', deleteBtn: false},]);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setCocktail(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setCocktail(prevState => ({...prevState, [name]: file}));
    };

    const addIng = (event) => {
        event.preventDefault()
        const ingredientsCopy = [...ingredients];
        ingredientsCopy.push({name: '', amount: '', deleteBtn: true},);
        setIngredients(ingredientsCopy);
    };
    const deleteIng = (index) => {
        const ingredientsCopy = [...ingredients];
        ingredientsCopy.splice(index, 1);
        setIngredients(ingredientsCopy);
    };
    const addIngName = (event, index) => {
        const ingredientsCopy = [...ingredients];
        ingredientsCopy[index].name = event.target.value;
        setIngredients(ingredientsCopy);
    };
    const addIngAmount = (event, index) => {
        const ingredientsCopy = [...ingredients];
        ingredientsCopy[index].amount = event.target.value;
        setIngredients(ingredientsCopy);
    };

    const ingList = ingredients.map((ing, index) => {
        return (
            <IngredientLine key={index} onIngNameChange={event => addIngName(event, index)}
                            onIngAmountChange={event => addIngAmount(event, index)}
                            remove={() => deleteIng(index)}
                            name={ingredients[index].name} amount={ingredients[index].amount}
                            deleteBtn={ingredients[index].deleteBtn}
            />);
    });

    return (
        <div className="container">
            <header>
                <h2>Create Cocktail</h2>
                <Nav
                    path="/" name="Cocktails"
                    path2="/myCocktails" name2="My Cocktails"
                />
            </header>
            <div className="cocktailCreator">
                <form className="content">
                    <p style={{marginBottom: 0}}>Cocktail name:</p>
                    <input
                        style={{marginTop: 10}} className="cocktailName" name="name"
                        value={cocktail.name} onChange={inputChangeHandler} placeholder="Add cocktail name"
                    />
                    <div className="ingredients">
                        <p>Ingredients:</p>
                        {ingList}
                        <button onClick={addIng} className="addIngBtn">Add ingredient</button>
                    </div>
                    <p style={{marginBottom: 0}}>Recipe:</p>
                    <textarea
                        className="cocktailRecipe" name="recipe"
                        value={cocktail.recipe} onChange={inputChangeHandler}
                    />
                    <p>Image:</p>
                    <input
                        type="file" onChange={fileChangeHandler}
                        accept=".jpg, .jpeg, .png" name="image"
                    />
                    {message && <p>{message}</p>}
                    <button
                        type="button" className="addCocktail"
                        onClick={submitFormHandler}
                    >Create cocktail</button>
                    {errors && <span className="error">{errors}</span>}
                </form>
            </div>
        </div>
    );
};

export default CreateCocktail;