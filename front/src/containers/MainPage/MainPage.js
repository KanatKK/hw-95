import React, {useEffect} from 'react';
import Nav from "../Nav/Nav";
import {useDispatch} from "react-redux";
import {getCocktails} from "../../store/actions";
import CocktailsList from "../../components/CocktailsList/CocktailsList";

const MainPage = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCocktails());
    }, [dispatch]);

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(getCocktails());
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch]);

    return (
        <div className="container">
            <header>
                <h2>Cocktails</h2>
                <Nav
                    path="/createCocktail" name="Create Cocktail"
                    path2="/myCocktails" name2="My Cocktails"
                />
            </header>
            <div className="content">
                <CocktailsList/>
            </div>
        </div>
    );
};

export default MainPage;