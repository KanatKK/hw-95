import React, {useEffect} from 'react';
import Nav from "../Nav/Nav";
import {useDispatch, useSelector} from "react-redux";
import {getMyCocktails} from "../../store/actions";
import CocktailsList from "../../components/CocktailsList/CocktailsList";

const MyCocktails = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);

    useEffect(() => {
        dispatch(getMyCocktails(user._id));
    }, [dispatch, user._id]);

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(getMyCocktails(user._id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, user._id]);

    return (
        <div className="container">
            <header>
                <h2>My Cocktails</h2>
                <Nav
                    path="/createCocktail" name="Create cocktail"
                    path2="/" name2="Cocktails"
                />
            </header>
            <div className="content">
                <CocktailsList/>
            </div>
        </div>
    );
};

export default MyCocktails;