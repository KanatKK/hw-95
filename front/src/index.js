import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/app/App';
import * as serviceWorker from './serviceWorker';
import store from "./store/configureStore";
import axiosApi from "./axiosApi";
import {Provider} from 'react-redux';

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authentication'] = store.getState().user.user.token;
    } catch(e) {
        // do nothing, no token exists
    }
    return config;
});

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <App />
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
