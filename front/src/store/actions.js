import {
    COCKTAIL_FAILURE,
    COCKTAIL_SUCCESS,
    CREATE_COCKTAIL,
    DELETE_COCKTAIL,
    FACEBOOK_LOGIN,
    GET_COCKTAILS,
    GET_COCKTAILS_SUCCESS,
    GET_MY_COCKTAILS,
    GET_MY_COCKTAILS_SUCCESS,
    GET_UNPUBLISHED_COCKTAILS,
    LOGIN_ADMIN,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    PUBLISH_COCKTAIL,
    UNPUBLISHED_COCKTAILS_SUCCESS,
    USER_FAILURE
} from "./actionTypes";

export const loginAdmin = admin => {
    return {type: LOGIN_ADMIN, admin};
};
export const loginUserSuccess = user => {
    return {type: LOGIN_USER_SUCCESS, user};
};
export const logoutUser = (user) => {
    return {type: LOGOUT_USER, user};
};
export const userFailure = error => {
    return {type: USER_FAILURE, error};
};
export const facebookLogin = data => {
    return {type: FACEBOOK_LOGIN, data};
};

export const createCocktail = cocktail => {
    return {type: CREATE_COCKTAIL, cocktail};
};
export const cocktailFailure = error => {
    return {type: COCKTAIL_FAILURE, error};
};
export const cocktailSuccess = message => {
    return {type: COCKTAIL_SUCCESS, message};
};

export const getUnpublishedCocktails = () => {
    return {type: GET_UNPUBLISHED_COCKTAILS}
}
export const unpublishedCocktailsSuccess = (cocktails) => {
    return {type: UNPUBLISHED_COCKTAILS_SUCCESS, cocktails}
}

export const getCocktails = () => {
    return {type: GET_COCKTAILS};
};
export const getCocktailsSuccess = (cocktails) => {
    return {type: GET_COCKTAILS_SUCCESS, cocktails};
};

export const getMyCocktails = (cocktails) => {
    return {type: GET_MY_COCKTAILS, cocktails};
};
export const getMyCocktailsSuccess = (cocktails) => {
    return {type: GET_MY_COCKTAILS_SUCCESS, cocktails};
};

export const deleteCocktail = cocktail => {
    return {type: DELETE_COCKTAIL, cocktail};
};
export const publishCocktail = cocktail => {
    return {type: PUBLISH_COCKTAIL, cocktail};
};