import {
    COCKTAIL_FAILURE,
    COCKTAIL_SUCCESS,
    GET_COCKTAILS_SUCCESS,
    GET_MY_COCKTAILS_SUCCESS,
    UNPUBLISHED_COCKTAILS_SUCCESS
} from "../actionTypes";

const initialState = {
    cocktails: null,
    errors: "",
    message: "",
};

const cocktails = (state = initialState, action) => {
    switch (action.type) {
        case GET_MY_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails, errors: ""};
        case GET_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails, errors: ""};
        case UNPUBLISHED_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails, errors: ""};
        case COCKTAIL_SUCCESS:
            return {...state, errors: "", message: action.message};
        case COCKTAIL_FAILURE:
            return {...state, errors: action.error};
        default:
            return state;
    }
};

export default cocktails;