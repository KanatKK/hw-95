import {LOGIN_USER_SUCCESS, LOGOUT_USER, USER_FAILURE} from "../actionTypes";

const initialState = {
    user: null,
    errors: "",
};

const user = (state = initialState, action) => {
    switch (action.type) {
        case USER_FAILURE:
            return {...state, errors: action.error};
        case LOGOUT_USER:
            return {...state, user: null};
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.user, errors: null};
        default:
            return state;
    }
};

export default user;