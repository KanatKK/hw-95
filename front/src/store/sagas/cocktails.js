import {put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
    cocktailFailure,
    cocktailSuccess,
    getCocktailsSuccess,
    getMyCocktailsSuccess,
    unpublishedCocktailsSuccess
} from "../actions";

export function* createCocktailSaga({cocktail}) {
    try {
        const response = yield axiosApi.post("/cocktails/", cocktail);
        yield put(cocktailSuccess(response.data.message));
    } catch (e) {
        if (e.response && e.response.data) {
            yield put(cocktailFailure(e.response.data.error));
        } else {
            yield put(cocktailFailure({global: "No internet"}));
        }
    }
}

export function* getCocktailsSaga() {
    try {
        const response = yield axiosApi.get("/cocktails");
        yield put(getCocktailsSuccess(response.data));
    } catch (e) {
        console.log(e);
    }
}

export function* getMyCocktailsSaga({cocktails}) {
    try {
        const response = yield axiosApi.get("/cocktails/myCocktails/" + cocktails);
        yield put(getMyCocktailsSuccess(response.data));
    } catch (e) {
        console.log(e);
    }
}

export function* getUnpublishedSaga() {
    try {
        const response = yield axiosApi.get("cocktails/unpublished");
        yield put(unpublishedCocktailsSuccess(response.data));
    } catch (e) {
        console.log(e);
    }
}

export function* deleteCocktailSaga({cocktail}) {
    try {
        yield axiosApi.delete("/cocktails/delete/" + cocktail);
    } catch (e) {
        console.log(e);
    }
}

export function* publishCocktailSaga({cocktail}) {
    try {
        yield axiosApi.put("/cocktails/publish/" + cocktail);
    } catch (e) {
        console.log(e);
    }
}