import {put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {loginUserSuccess, logoutUser, userFailure} from "../actions";

export function* facebookLoginSaga({data}) {
    try {
        const response = yield axiosApi.post("/users/facebookLogin", data);
        yield put(loginUserSuccess(response.data));
    } catch(e) {
        yield put(userFailure(e.response.data));
    }
}

export function* loginAdminSaga({admin}) {
    try {
        const response = yield axiosApi.post("/users/sessions", admin);
        yield put(loginUserSuccess(response.data));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(userFailure(e.response.data.error));
        } else {
            yield put(userFailure({global: "No internet"}));
        }
    }
}

export function* logoutUserSaga({user}) {
    if (user) {
        yield axiosApi.delete("/users/sessions");
        yield put(logoutUser());
    }
}