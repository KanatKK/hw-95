import {takeEvery} from "redux-saga/effects";
import {
    CREATE_COCKTAIL,
    DELETE_COCKTAIL,
    FACEBOOK_LOGIN, GET_COCKTAILS, GET_MY_COCKTAILS,
    GET_UNPUBLISHED_COCKTAILS,
    LOGIN_ADMIN,
    LOGOUT_USER, PUBLISH_COCKTAIL
} from "../actionTypes";
import {facebookLoginSaga, loginAdminSaga, logoutUserSaga} from "./user";
import {
    createCocktailSaga,
    deleteCocktailSaga,
    getCocktailsSaga, getMyCocktailsSaga,
    getUnpublishedSaga,
    publishCocktailSaga
} from "./cocktails";

export function* rootSaga() {
    yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
    yield takeEvery(LOGOUT_USER, logoutUserSaga);
    yield takeEvery(LOGIN_ADMIN, loginAdminSaga);
    yield takeEvery(CREATE_COCKTAIL, createCocktailSaga);
    yield takeEvery(GET_UNPUBLISHED_COCKTAILS, getUnpublishedSaga);
    yield takeEvery(DELETE_COCKTAIL, deleteCocktailSaga);
    yield takeEvery(PUBLISH_COCKTAIL, publishCocktailSaga);
    yield takeEvery(GET_COCKTAILS, getCocktailsSaga);
    yield takeEvery(GET_MY_COCKTAILS, getMyCocktailsSaga);
}